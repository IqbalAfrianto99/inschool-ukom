@extends('template')
    
@section('content')
    
    <div class="row">
        {{-- Users Name --}}
        <div class="col-12 mt-5">
            <h2 class="mb-0">Hi, Slim My Man!</h2>
            <p class="text-muted lead mt-1">Ubah informasi tentang data diri anda disini</p>
        </div>
    </div>
    
    <div class="row mt-5">
        {{-- Users current data --}}
        <div class="col-sm-12 col-md-4 mt-5 mb-4">
            <div class="card">
                <div class="card-body text-center">
                    {{-- User profile --}}
                    <img src="{{ asset('img/no-photo.png') }}" alt="" class="img-center img-profile choose-photo" id="img-file" title="Ubah foto profile">
                    <p class="h5 font-weight-bold">Slim My Man</p>
                    <p class="text-muted">Jl. Kalong Elit No.39 RT 08/02,Kec. Rawalumbu, Bekasi 17145</p>
                    <p class="text-muted font-weight-bold">081398705478</p>
                </div>
            </div>
        </div>
        {{-- Edit profile form --}}
        <div class="col-sm-12 col-md-8 mt-1 mb-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Edit Profile</h5>
                    <hr>
                    <form action="">
                        <input type="file" name="picture" id="" class="d-none img-preview">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" name="name" id="name" class="form-control rounded-100" placeholder="Nama Lengkap*">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="username" id="username" class="form-control rounded-100 disabled" disabled value="Slim7878">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="number" name="nip" id="nip" class="form-control rounded-100" disabled value="198503302003121002">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="password" name="password" id="password" class="form-control rounded-100" placeholder="Password*">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="number" name="phone" id="phone" class="form-control rounded-100" placeholder="No Telp*">
                            </div>
                            <div class="form-group col-md-12">
                                <textarea name="address" id="address" class="form-control" rows="3" placeholder="Alamat*"></textarea>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="form-group col-md-3 offset-md-9">
                                <input type="submit" value="Simpan" class="form-control rounded-100 bg-theme">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection