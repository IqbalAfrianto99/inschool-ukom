<div class="col-lg-2 sidebar pr-0">
    <ul class="nav flex-column">
        <li class="menu-header">Dashboard</li>
        <li class="nav-item" id="beranda">
            <a href="{{ route('operator.index') }}" class="nav-link">
                <i class="fa fa-tachometer"></i>
                Dashboard
            </a>
        </li>
        <li class="menu-header">Modul</li>
        <li class="nav-item has-submenu" id="inventaris">
            <a href="javascript:void()" class="nav-link">
                <i class="fa fa-dropbox"></i>
                Inventaris
                <i class="fa {{ Request::segment(2) == "inventaris" ? "fa-minus" : "fa-plus" }} ml-2 icon-add"></i>
            </a>
            <ul class="submenu list-unstyled" style="display: {{ Request::segment(2) == 'inventaris' ? "block" : "none"}}">
                <li class="nav-item">
                    <a href="{{ route('operator.invent.index') }}" class="inventarisir">Inventarisir</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('operator.invent.room') }}" class="ruangan">Ruangan</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('operator.invent.type') }}" class="tipe">Tipe Barang</a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-submenu" id="peminjaman">
            <a href="javascript:void()" class="nav-link">
                <i class="fa fa-clipboard"></i>
                Peminjaman
                <i class="fa {{ Request::segment(2) == "peminjaman" ? "fa-minus" : "fa-plus" }} ml-2 icon-add"></i>
            </a>
            <ul class="submenu list-unstyled" style="display: {{ Request::segment(2) == 'peminjaman' ? "block" : "none"}}">
                <li class="nav-item">
                    <a href="{{ route('operator.borrow.index') }}" class="daftar">Daftar Peminjaman</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('operator.borrow.request') }}" class="permintaan">Permintaan Peminjaman</a>
                </li>
            </ul>
        </li>
    </ul>
</div