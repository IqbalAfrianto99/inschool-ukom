<div class="modal-header">
    <h6 class="modal-title mb-0">Detail Barang</h6>
</div>

<div class="modal-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-4 pl-0">
                <img src="{{ asset('img/no-photo.png') }}" class="img-fluid">
            </div>
            <div class="col-8 pr-0">
                <div class="row">
                    <div class="col-6 mb-3">
                        <small class="text-bold">Nama</small>
                        <div class="modal-text">Acer ROG XXX</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Ruangan</small>
                        <div class="modal-text">Ruangan D8</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Tipe Barang</small>
                        <div class="modal-text">Teknologi</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Kode Barang</small>
                        <div class="modal-text">A1-0001</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Kondisi</small>
                        <div class="modal-text">Baik</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Stok</small>
                        <div class="modal-text">10</div>
                    </div>
                    <div class="col-12 mb-3">
                        <small class="text-bold">Deskripsi</small>
                        <div>Laptop untuk programming</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer py-3">
    <button class="btn btn-base" onclick="$('.modal').modal('hide')">Tutup</button>
</div>