@extends('template')

@section('content')
    
    <div class="row mt-3">
        <div class="col-lg-12">

            <div class="card card-base">
                <h6 class="card-header">Form Tambah Barang</h6>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            <form action="" method="post">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="choose-photo bg-light">
                                            <img src="" class="img-fluid" id="img-file">
                                        </div>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="file" name="picture" class="img-preview d-none">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Nama barang</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Tipe barang</label>
                                                    <select name="" id="" class="form-control">
                                                        <option value="">A</option>
                                                        <option value="">B</option>
                                                        <option value="">C</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Kode barang</label>
                                                    <div class="input-group input-group-alternative mb-4">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">A1</i></span>
                                                        </div>
                                                        <input class="form-control form-control-alternative" type="text">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Ruangan</label>
                                                    <select name="" id="" class="form-control">
                                                        <option value="">A</option>
                                                        <option value="">B</option>
                                                        <option value="">C</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Kondisi barang</label>
                                                    <select name="" id="" class="form-control">
                                                        <option value="">A</option>
                                                        <option value="">B</option>
                                                        <option value="">C</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Stok</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('admin.invent.index') }}" class="btn btn-secondary btn-rounded">Cancel</a>
                                        &nbsp;
                                        <input type="submit" class="btn btn-base btn-rounded">
                                    </div>
                                </div>                
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection