@extends('auth')

@section('content')

    {{-- Main content --}}
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-md-9 col-sm-12 m-auto">
                
                <div class="content full-width-xs">
                    <div class="vertical-align">
                        {{-- Brand image --}}
                        <img src=" {{ asset('img/logo/inschool-text.png') }} " alt="" style="height: 15vh"><br>
                        <span class="font-weight-bold">Daftar sekarang untuk mulai menggunakan INSCHOOL</span>
                        <div class="card card-border-top mt-2 mb-3">
                            <div class="card-body">
                                <h4 class="card-title">Register</h4>
                                <hr>
                                {{-- Register form --}}
                                <form action="{{ route('auth.save') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('name') ? 'has-danger' : '' }}">
                                                <input type="text" name="name" id="name" class="form-control rounded-100 {{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="Nama Lengkap*" value="{{ old('name') }}">
                                                @if($errors->has('name'))
                                                    <span class="invalid-feedback text-left" role="alert">
                                                        <strong>{{ $errors->first('name')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('username') ? 'has-danger' : '' }}">
                                                <input type="text" name="username" id="username" class="form-control rounded-100 {{ $errors->has('username') ? 'is-invalid' : '' }}" placeholder="Username*" value="{{ old('username') }}">
                                                @if($errors->has('username'))
                                                    <span class="invalid-feedback text-left" role="alert">
                                                        <strong>{{ $errors->first('username')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('password') ? 'has-danger' : '' }}">
                                                <input type="password" name="password" id="password" class="form-control rounded-100 {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Password*" value="{{ old('password') }}">
                                                @if($errors->has('password'))
                                                    <span class="invalid-feedback text-left" role="alert">
                                                        <strong>{{ $errors->first('password')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('phone') ? 'has-danger' : '' }}">
                                                <input type="number" name="phone" id="phone" class="form-control rounded-100 {{ $errors->has('phone') ? 'is-invalid' : '' }}" placeholder="No Telp*" value="{{ old('phone') }}">
                                                @if($errors->has('phone'))
                                                    <span class="invalid-feedback text-left" role="alert">
                                                        <strong>{{ $errors->first('phone')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('nip') ? 'has-danger' : '' }}">
                                                <input type="number" name="nip" id="nip" class="form-control rounded-100 {{ $errors->has('nip') ? 'is-invalid' : '' }}" placeholder="NIP" value="{{ old('nip') }}">
                                                @if($errors->has('nip'))
                                                    <span class="invalid-feedback text-left" role="alert">
                                                        <strong>{{ $errors->first('nip')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group {{ $errors->has('address') ? 'has-danger' : '' }}">
                                                <textarea name="address" id="address" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" rows="3" placeholder="Alamat*">{{ old('name') }}</textarea>
                                                @if($errors->has('address'))
                                                    <span class="invalid-feedback text-left" role="alert">
                                                        <strong>{{ $errors->first('address')}}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            {{-- Register button --}}
                                            <div class="form-group col-md-12">
                                                <input type="submit" value="Daftar" class="form-control rounded-100 bg-theme">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                {{-- End of register form --}}
                                <center>
                                    {{-- Link to login page --}}
                                    <h5>Sudah mempunyai akun? <a href="{{ route('auth.login') }}">Masuk sekarang</a></h5>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@push('script')
    <script>
        if({{ session('status') ?? false }}){
            swal({
                title : 'Pendaftran Berhasil',
                text : "{{ session('msg') }}",
                icon: 'success',
                button: true
            });
        }
    </script>
@endpush