@extends('template')

@section('content')
    
    <div class="row mt-3">
        <div class="col-lg-12">

            <div class="card card-base">
                <h6 class="card-header">Form Tambah Pengguna</h6>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            <form action="" method="post">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="choose-photo bg-light">
                                            <img src="" class="img-fluid" id="img-file">
                                        </div>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="file" name="" id="" class="img-preview d-none">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">NIP</label>
                                                    <input type="text" class="form-control" autofocus>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Nama Pengguna</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Level</label>
                                                    <select name="" class="form-control">
                                                        <option value="">Peminjam</option>
                                                        <option value="">Operator</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Username</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Password</label>
                                                    <input type="password" name="" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Nomor Telepon</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Alamat</label>
                                                    <textarea name="" rows="3" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Status</label>
                                                    <select name="" class="form-control">
                                                        <option value="">Aktif</option>
                                                        <option value="">Tidak Aktif</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('admin.user.index') }}" class="btn btn-secondary btn-rounded">Cancel</a>
                                        &nbsp;
                                        <input type="submit" class="btn btn-base btn-rounded">
                                    </div>
                                </div>                
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection