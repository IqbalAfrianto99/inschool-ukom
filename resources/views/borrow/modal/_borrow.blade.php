<div class="modal-header">
    <h6 class="modal-title mb-0">Detail Barang</h6>
</div>

<div class="modal-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-4 pl-0">
                <img src="{{ asset('img/no-photo.png') }}" class="img-fluid">
            </div>
            <div class="col-8 pr-0">
                <div class="row">
                    <div class="col-6 mb-3">
                        <small class="text-bold">Nama Peminjam</small>
                        <div class="modal-text">Slim My Man</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Barang</small>
                        <div class="modal-text">Acer ROG XXX</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Jumlah</small>
                        <div class="modal-text text-bold">1</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Status</small>
                        <div class="modal-text">Dipinjam</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Waktu Peminjaman</small>
                        <div class="modal-text">23 September 2001</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Waktu Pengembalian</small>
                        <div class="modal-text">25 September 2001</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer py-3">
    <button class="btn btn-base" onclick="$('.modal').modal('hide')">Tutup</button>
</div>