@extends('template')

@section('content')

    {{-- Inventory list card --}}
    <div class="row mt-5">
        <div class="col-sm-12 col-md-3 mt-3">
            <a href="{{ route('peminjam.invent.detail', 45) }}">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('img/epson-printer.jpeg') }}" alt="" class="img-fluid">
                        <div class="mt-5">
                            <h5 class="card-title mb-0">Printer Epson L330</h5>
                            <small class="text-muted font-weight-bold">Jenis: Printer, Kondisi: Baik</small>
                        </div>
                        <p class="text-dark mt-3 mb-0">Stok: 5</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-12 col-md-3 mt-3">
            <a href="#">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('img/epson-printer.jpeg') }}" alt="" class="img-fluid">
                        <div class="mt-5">
                            <h5 class="card-title mb-0">Printer Epson L331</h5>
                            <small class="text-muted font-weight-bold">Jenis: Printer, Kondisi: Baik</small>
                        </div>
                        <p class="text-dark mt-3 mb-0">Stok: 5</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-12 col-md-3 mt-3">
            <a href="#">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('img/epson-printer.jpeg') }}" alt="" class="img-fluid">
                        <div class="mt-5">
                            <h5 class="card-title mb-0">Printer Epson L332</h5>
                            <small class="text-muted font-weight-bold">Jenis: Printer, Kondisi: Baik</small>
                        </div>
                        <p class="text-dark mt-3 mb-0">Stok: 5</p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-12 col-md-3 mt-3">
            <a href="#">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('img/epson-printer.jpeg') }}" alt="" class="img-fluid">
                        <div class="mt-5">
                            <h5 class="card-title mb-0">Printer Epson L333</h5>
                            <small class="text-muted font-weight-bold">Jenis: Printer, Kondisi: Baik</small>
                        </div>
                        <p class="text-dark mt-3 mb-0">Stok: 5</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
    {{-- End of inventory list card --}}
    
@endsection