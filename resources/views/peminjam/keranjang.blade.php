@extends('template')

@section('content')

    <div class="card mt-4">
        <div class="card-body">
            {{-- Cart table --}}
            <div class="table-responsive mt-4">
                <table class="table align-items-center">
                    <thead class="bg-theme  text-center">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Kode Barang</th>
                            <th scope="col">Kondisi</th>
                            <th scope="col" width="120">Qty</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <tr>
                            <td>1</td>
                            <td>Laptop Thinkpad</td>
                            <td>A1-254</td>
                            <td>Baik</td>
                            <td>
                                <input type="number" name="qty" id="qty" min="1" class="form-control mx-auto">    
                            </td> 
                            <td>
                                <a href="#" class="btn btn-danger rounded-100">Hapus</a>    
                            </td>   
                        </tr>    
                        <tr>
                            <td>2</td>
                            <td>Proyektor Infocus T678</td>
                            <td>C1-623</td>
                            <td>Baik</td>
                            <td>
                                <input type="number" name="qty" id="qty" min="1" class="form-control mx-auto">    
                            </td> 
                            <td>
                                <a href="#" class="btn btn-danger rounded-100">Hapus</a>    
                            </td>   
                        </tr>    
                        <tr>
                            <td>3</td>
                            <td>Printer Epson L330</td>
                            <td>B1-980</td>
                            <td>Baik</td>
                            <td>
                                <input type="number" name="qty" id="qty" min="1" class="form-control mx-auto">    
                            </td> 
                            <td>
                                <a href="#" class="btn btn-danger rounded-100">Hapus</a>    
                            </td>   
                        </tr>
                        <tr>
                            <td colspan="6" align="right">
                                <a href="" class="btn bg-theme rounded-100">Perbaharui</a>    
                            </td>    
                        </tr>    
                    </tbody>    
                </table>
            </div>
            {{-- End of cart table --}} 
        </div>
    </div>

    {{-- Cart overview --}}
    <div class="row mt-5">
        <div class="col-md-4 ml-auto">
            <div class="card">
                <div class="card-body">
                    <span class="font-weight-bold"> Slim My Man</span>
                    <p class="mt-3">Total barang yang dipinjam <span class="float-right">03</span></p>
                    <a href="#" class="btn bg-theme rounded-100 mt-2 float-right">Lanjutkan peminjaman</a>
                </div>
            </div>
        </div>
    </div>
    {{-- End of cart overview --}}

@endsection