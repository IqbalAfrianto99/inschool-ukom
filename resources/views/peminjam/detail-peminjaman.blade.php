@extends('template')

@section('content')

    <div class="row">
        <div class="col-md-8 col-sm-12">
            {{-- Borrow detail card --}}
            <div class="card mt-5">
                <div class="card-body">
                    <span class="rounded-100 bg-yellow p-3">Pending</span>

                    <p class="font-weight-bold mt-5">Barang yang dipinjam</p>
                    <table border="0" class="table mt-5">
                        <tr>
                            <td>1.</td>
                            <td>Laptop Thinkpad</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Proyektor Infocus</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>Printer Epson L300</td>
                            <td>1</td>
                        </tr>
                    </table>
                </div>
            </div>
            {{-- End of borrow detail --}}
        </div>
        <div class="col-md-4 col-sm-12 mt-5">
            {{-- Borrower detail card --}}
            <div class="card">
                <div class="card-body">
                    <span class="font-weight-bold mb-4">Slim My Man</span>
                    <p class="mt-3">Tanggal pinjam <span class="float-right">21/02/2019</span></p>
                    <p class="mt-3">Total barang yang dipinjam <span class="float-right">05</span></p>
                </div>
            </div>
            {{-- End of borrower detail card --}}
        </div>
    </div>
    
@endsection