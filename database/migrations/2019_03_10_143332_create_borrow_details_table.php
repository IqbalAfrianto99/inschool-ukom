<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBorrowDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrow_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('borrow_id');
            $table->unsignedInteger('inventory_id');
            $table->unsignedInteger('qty');
            $table->enum('status', [
                'Pending', 'Ditolak', 'Siap', 'Dipinjam', 'Belum Dikembalikan', 'Sudah Dikembalikan'
            ])->default('Pending');
            $table->timestamps();

            $table->index(['borrow_id', 'inventory_id']);
            $table->foreign('borrow_id')->on('borrows')->references('id');
            $table->foreign('inventory_id')->on('inventories')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrow_details');
    }
}
