function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img-file').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
    $('.has-submenu').click(function () {
        var $icon = $(this).find('.icon-add');
        $icon.hasClass('fa-plus') ? $icon.removeClass('fa-plus').addClass('fa-minus') : $icon.addClass('fa-plus').removeClass('fa-minus');
        $(this).find('.submenu').slideToggle('slow');
    });

    $('.choose-photo').click(function () {
        $('.img-preview').trigger('click');
    })

    $('.img-preview').change(function () {
        readURL(this);
    })

})
