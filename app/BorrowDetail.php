<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Borrow;
use App\Inventory;

class BorrowDetail extends Model
{
    protected $fillable = ['borrow_id', 'inventory_id', 'qty', 'status'];

    public function borrow()
    {
        return $this->belongsTo(Borrow::class);
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }
}
