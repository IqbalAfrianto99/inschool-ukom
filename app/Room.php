<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Inventory;

class Room extends Model
{
    protected $fillable = ['code', 'name', 'desc'];

    public function inventory ()
    {
        return $this->hasMany(Inventory::class);
    }
}
