<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Inventory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'condition' => 'required',
            'desc' => 'required',
            'qty' => 'required|numeric',
            'picture' => 'required|image|max:10000'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama barang harus diisi',
            'condition.required' => 'Kondisi barang harus diisi',
            'desc.required' => 'Deskripsi harus diisi',
            'qty.required' => 'Jumlah barang harus diisi',
            'qty.numeric' => 'Jumlah barang harus diisi dengan angka',
            'picture.required' => 'Foto barang harus diisi',
            'picture.image' => 'Format file tidak didukung',
            'picture.max' => 'File terlalu besar'
        ];
    }
}
