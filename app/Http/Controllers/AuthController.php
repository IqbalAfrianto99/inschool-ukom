<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Validator;
use App\User;
use App\Http\Requests\Login;
use App\Http\Requests\Register;

class AuthController extends Controller
{

    public function login () {
        if(Session::get('role') == 1) {
            return redirect('/');
        } elseif(Session::get('role') == 2) {
            return redirect()->route('dashboard');
        } else {
            return view('login');
        }
    }
    
    public function register () {
        if(Session::get('role') == 1) {
            return redirect('/');
        } elseif(Session::get('role') == 2) {
            return redirect()->route('product.list');
        } else {
            return view('register');
        }
    }

    public function save (Register $request) {
        $user = User::create([
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'level_id' => 1 
        ])->borrower()->create([
            'nip' => $request->nip,
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address
         ]);

        return redirect()->route('auth.register')->with([
            'msg' => 'Silahkan hubungi admin atau operator',
            'status' => true
        ]);
    }

    public function submit (Login $request) { 

        if(Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            Session::put('id',Auth::user()->id);
            Session::put('username', Auth::user()->username);
            Session::put('role', Auth::user()->level_id);

            if(Auth::user()->approved){
                if(Auth::user()->level_id == 1){
                    Session::put('name', Auth::user()->borrower->name);
                    return redirect()->route('peminjam.index');
                }elseif(Auth::user()->level_id == 2){
                    Session::put('name', Auth::user()->operator->name);
                    return redirect()->route('operator.index');
                }elseif(Auth::user()->level_id == 3){
                    Session::put('name', Auth::user()->admin->name);
                    return redirect()->route('admin.index');
                }
            }else{
                $msg = "Silahkan hubungi admin atau operator untuk konfirmasi akun anda";
                return view('auth.login', compact('msg'));
            }
        } else {
            $msg = "Username atau password salah";
            return view('auth.login', compact('msg'));
        }
    }

    public function logout () {
        Auth::logout();
        return redirect()->route('auth.login');
    }
}
