<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Admin extends Model
{
    protected $fillable = ['user_id', 'name'];

    public function user ()
    {
        return $this->belongsTo(User::class);
    }
}
