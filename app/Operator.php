<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Operator extends Model
{
    protected $fillable = ['user_id', 'name', 'nip', 'ip_address'];

    public function user ()
    {
        return $this->belongsTo(User::class);
    }
}
