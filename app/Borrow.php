<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Borrower;
use App\Inventory;
use App\BorrowDetail;

class Borrow extends Model
{
    protected $fillable = ['borrower_id', 'borrow_at', 'return_at'];

    public function borrower ()
    {
        return $this->belongsTo(Borrower::class);
    }

    public function inventory ()
    {
        return $this->belongToMany(Inventory::class);
    }

    public function borrowDetail () 
    {
        return $this->hasMany(BorrowDetail::class);
    }
}
