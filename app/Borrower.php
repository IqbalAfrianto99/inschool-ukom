<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Borrow;

class Borrower extends Model
{
    protected $fillable = ['user_id', 'nip', 'name', 'phone', 'address'];

    public function user ()
    {
        return $this->belongsTo(User::class);
    }

    public function borrow ()
    {
        return $this->hasMany(Borrow::class);
    }
}
