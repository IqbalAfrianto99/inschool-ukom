<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Inventory;

class Type extends Model
{
    protected $fillable = ['code', 'name', 'desc'];

    public function inventory ()
    {
        return $this->hasMany(Inventory::class);
    }
}
