<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Carbon\Carbon;

setlocale(LC_ALL, 'IND');

Route::get('/', function () { 
    return redirect()->route('auth.login');
});

Route::get('/u', 'AdminController@chart')->name('chart');

Route::get('tes', function () {
    // how to get ip
    $ip = gethostbyname(gethostname());    
    
    // how to use Cart
    Cart::add(['id' => '293ad', 'name' => 'Product 1', 'qty' => 1, 'price' => 9.99, 'options' => ['size' => 'large']]);
    dd(Cart::content());

    // how to use activity
    // activity()
    //     ->withProperties([
    //         'object' => 'Acer ROG XXX', 
    //         'by' => '<b>Slim My Man</b>'
    //     ])
    //     ->log('ingin melakukan peminjaman');
    // $act = Activity::all()->last();
    // $time = $act->created_date;
    
    // echo $act->getExtraProperty('by').' dengan ip address '.$ip.' '.$act->description.' "'.$act->getExtraProperty('object').'" pada '.$time.'.';
});

// Daftar Pengguna
Route::post('apiuser', function () {
    $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
    $act .= '<i class="fa fa-ellipsis-h"></i>';
    $act .= '</a>';
    $act .= '<div class="dropdown-menu" style="width:50px;">';
    $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="showData(1)">Detail</a>';
    $act .= '<a class="dropdown-item" href="'.route("admin.user.edit", 1).'">Edit</a>';
    $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteData(1)">Delete</a>';
    $act .= '</div>';
    $data = array(
        array(
            'picture' => '<img src="'.asset('img/no-photo.png').'" class="img-round img-thumb">',
            'name' => 'Slim My Man',
            'nip' => '123456789101112311',
            'telepon' => '081393300816',
            'level' => '<span class="badge badge-pill badge-primary">Peminjam</span>',
            'action' => $act
        ),
        array(
            'picture' => '<img src="'.asset('img/no-photo.png').'" class="img-round img-thumb">',
            'name' => 'M Qadafi',
            'nip' => '123456789101112309',
            'telepon' => '081393300816',
            'level' => '<span class="badge badge-pill badge-primary">Peminjam</span>',
            'action' => $act
        )
    );

    echo json_encode(array("sEcho" => 1, "aaData" => $data));
});

// Daftar Permintaan Pengguna
Route::post('apiusereq', function () {
    $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
    $act .= '<i class="fa fa-ellipsis-h"></i>';
    $act .= '</a>';
    $act .= '<div class="dropdown-menu" style="width:50px;">';
    $act .= '<a class="dropdown-item" href="javascript:void(0)">Terima</a>';
    $act .= '<a class="dropdown-item" href="javascript:void(0)">Tolak</a>';
    $act .= '</div>';
    $data = array(
        array(
            'picture' => '<img src="'.asset('img/no-photo.png').'" class="img-round img-thumb">',
            'name' => 'Slim My Man',
            'nip' => '123456789101112311',
            'telepon' => '081393300816',
            'status' => '<span class="badge badge-pill badge-primary">Tidak Aktif</span>',
            'action' => $act
        ),
        array(
            'picture' => '<img src="'.asset('img/no-photo.png').'" class="img-round img-thumb">',
            'name' => 'M Qadafi',
            'nip' => '123456789101112309',
            'telepon' => '081393300816',
            'status' => '<span class="badge badge-pill badge-primary">Tidak Aktif</span>',
            'action' => $act
        )
    );

    echo json_encode(array("sEcho" => 1, "aaData" => $data));
});

// Daftar Inventaris
Route::post('apiinvent', function () {
    $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
    $act .= '<i class="fa fa-ellipsis-h"></i>';
    $act .= '</a>';
    $act .= '<div class="dropdown-menu" style="width:50px;">';
    $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="showData(1)">Detail</a>';
    $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="printLabel(\'A1-0001\')">Print Label</a>';
    $act .= '<a class="dropdown-item" href="'.route("admin.invent.edit_invent", 1).'">Edit</a>';
    $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteData(1)">Delete</a>';
    $act .= '</div>';
    $data = array(
        array(
            'picture' => '<img src="'.asset('img/no-photo.png').'" class="img-round img-thumb">',
            'name' => 'Acer ROG XXX',
            'room_id' => 'Ruangan D8',
            'code' => 'A1-0001',
            'condition' => '<span class="badge badge-pill badge-primary">Baik</span>',
            'qty' => '10',
            'action' => $act
        ),
        array(
            'picture' => '<img src="'.asset('img/no-photo.png').'" class="img-round img-thumb">',
            'name' => 'Barcode Scanner',
            'room_id' => 'Ruangan D8',
            'code' => 'B1-0001',
            'condition' => '<span class="badge badge-pill badge-primary">Baik</span>',
            'qty' => '1',
            'action' => $act
        )
    );

    echo json_encode(array("sEcho" => 1, "aaData" => $data));
});

// Daftar Ruangan
Route::post('apiroom', function () {
    $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
    $act .= '<i class="fa fa-ellipsis-h"></i>';
    $act .= '</a>';
    $act .= '<div class="dropdown-menu" style="width:50px;">';
    // if(Auth::user()->level_id === 3) {
        $act .= '<a class="dropdown-item" href="'.route("admin.invent.edit_room", 1).'">Edit</a>';
        $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteData(1)">Delete</a>';
    // }
    $act .= '</div>';
    $data = array(
        array(
            'code' => 'D9',
            'name' => 'Ruang 28',
            'desc' => 'Ruang penyimpanan jodoh',
            'action' => $act
        ),
        array(
            'code' => 'D10',
            'name' => 'Ruang 29',
            'desc' => 'Ruang penyimpanan hati',
            'action' => $act
        )
    );

    echo json_encode(array("sEcho" => 1, "aaData" => $data));
});

// Daftar Tipe Barang
Route::post('apitype', function () {
    $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
    $act .= '<i class="fa fa-ellipsis-h"></i>';
    $act .= '</a>';
    $act .= '<div class="dropdown-menu" style="width:50px;">';
    // if(Auth::user()->level_id === 3) {
        $act .= '<a class="dropdown-item" href="'.route("admin.invent.edit_type", 1).'">Edit</a>';
        $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteData(1)">Delete</a>';
    // }
    $act .= '</div>';
    $data = array(
        array(
            'code' => 'A1',
            'name' => 'Teknologi',
            'desc' => 'Contoh: Komputer, Monitor, Mouse, Keyboard, dst.',
            'action' => $act
        ),
        array(
            'code' => 'A2',
            'name' => 'Elektronik',
            'desc' => 'Contoh: Setrika, Kabel Roll, dst.',
            'action' => $act
        )
    );

    echo json_encode(array("sEcho" => 1, "aaData" => $data));
});

// Daftar Peminjaman
Route::post('apiborrow', function () {
    $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
    $act .= '<i class="fa fa-ellipsis-h"></i>';
    $act .= '</a>';
    $act .= '<div class="dropdown-menu" style="width:50px;">';
    $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="showData(1)">Detail</a>';
    $act .= '<a class="dropdown-item" href="'.route("admin.borrow.edit", 1).'">Edit</a>';
    $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteData(1)">Delete</a>';
    $act .= '</div>';
    $data = array(
        array(
            'picture' => '<img src="'.asset('img/no-photo.png').'" class="img-round img-thumb">',
            'name' => 'Slim My Man',
            'stuff' => 'Acer ROG XXX',
            'qty' => '1',
            'status' => '<span class="badge badge-pill badge-primary">DIPINJAM</span>',
            'borrow_at' => '23 September 2001',
            'action' => $act
        ),
        array(
            'picture' => '<img src="'.asset('img/no-photo.png').'" class="img-round img-thumb">',
            'name' => 'M Qadafi',
            'stuff' => 'Scanner',
            'qty' => '1',
            'status' => '<span class="badge badge-pill badge-primary">DIPINJAM</span>',
            'borrow_at' => '23 September 2001',
            'action' => $act
        )
    );

    echo json_encode(array("sEcho" => 1, "aaData" => $data));
});

// Daftar Permintaan Peminjaman
Route::post('apiborreq', function () {
    $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
    $act .= '<i class="fa fa-ellipsis-h"></i>';
    $act .= '</a>';
    $act .= '<div class="dropdown-menu" style="width:50px;">';
    $act .= '<a class="dropdown-item" href="javascript:void(0)">Terima</a>';
    $act .= '<a class="dropdown-item" href="javascript:void(0)">Tolak</a>';
    $act .= '</div>';
    $data = array(
        array(
            'picture' => '<img src="'.asset('img/no-photo.png').'" class="img-round img-thumb">',
            'name' => 'Slim My Man',
            'stuff' => 'Acer ROG XXX',
            'qty' => '1',
            'status' => '<span class="badge badge-pill badge-info">PENDING</span>',
            'borrow_at' => '23 September 2001',
            'action' => $act
        ),
        array(
            'picture' => '<img src="'.asset('img/no-photo.png').'" class="img-round img-thumb">',
            'name' => 'M Qadafi',
            'stuff' => 'Scanner',
            'qty' => '1',
            'status' => '<span class="badge badge-pill badge-info">PENDING</span>',
            'borrow_at' => '23 September 2001',
            'action' => $act
        )
    );

    echo json_encode(array("sEcho" => 1, "aaData" => $data));
});

// Daftar Peminjaman (Peminjam)
Route::post('apipeminjam', function () {
    $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
    $act .= '<i class="fa fa-ellipsis-h"></i>';
    $act .= '</a>';
    $act .= '<div class="dropdown-menu" style="width:50px;">';
    $act .= '<a class="dropdown-item" href="'.route('peminjam.borrow.detail', 45).'">Detail</a>';
    $act .= '</div>';
    $data = array(
        array(
            'id' => '51242',
            'qty' => 3,
            'borrow_at' => '20/02/2019',
            'status' => '<span class="badge badge-pill badge-default">PENDING</span>',
            'action' => $act
        ),
        array(
            'id' => '51244',
            'qty' => 3,
            'borrow_at' => '25/02/2019',
            'status' => '<span class="badge badge-pill badge-default">PENDING</span>',
            'action' => $act
        )
    );

    echo json_encode(array("sEcho" => 1, "aaData" => $data));
});

// Auth Section
Route::prefix('auth')->name('auth.')->group(function () {
    Route::get('login', function () {
        return view('auth/login');
    })->name('login')->middleware('guest');
    
    Route::get('register', function () {
        return view('auth/register');
    })->name('register')->middleware('guest');
});

// Modal API
// Route::prefix('modal')->middleware('auth')->name('modal.')->group(function () {
Route::prefix('modal')->name('modal.')->group(function () {
    Route::post('user', function () {
        return view('user/modal/_detail');
    })->name('user');

    Route::post('invent', function () {
        return view('inventory/modal/_invent');
    })->name('invent');

    Route::post('borrow', function () {
        return view('borrow/modal/_borrow');
    })->name('borrow');
});

// Route::prefix('admin')->name('admin.')->middleware('admin')->group(function () {
Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/', 'AdminController@index')->name('index');

    Route::get('profile', function () {
        return view('profile');
    })->name('profile');

    Route::prefix('inventaris')->name('invent.')->group(function () {
        Route::get('inventarisir', function () {
            return view('inventory/index');
        })->name('index');

        Route::get('ruangan', function () {
            return view('inventory/room');
        })->name('room');

        Route::get('tipe', function () {
            return view('inventory/type');
        })->name('type');

        Route::get('tambah-barang', function () {
            return view('inventory/create__invent');
        })->name('create_invent');

        Route::get('edit-barang/{slug}', function () {
            return view('inventory/create__invent');
        })->name('edit_invent');

        Route::get('tambah-ruangan', function () {
            return view('inventory/create__room');
        })->name('create_room');

        Route::get('edit-ruangan/{id}', function () {
            return view('inventory/create__room');
        })->name('edit_room');

        Route::get('tambah-tipe', function () {
            return view('inventory/create__type');
        })->name('create_type');

        Route::get('edit-tipe/{id}', function () {
            return view('inventory/create__type');
        })->name('edit_type');

        Route::get('label/{code}', function ($code) {
            $barcode = new BarcodeGenerator();
            $barcode->setText($code);
            $barcode->setType(BarcodeGenerator::Code128);
            $barcode->setScale(2);
            $barcode->setThickness(25);
            $barcode->setFontSize(10);
            $code = $barcode->generate();

            return view('inventory/label', compact('code'));
        })->name('label');

        Route::get('report', function () {
            return view('admin/report');
        })->name('report');
    });

    Route::prefix('peminjaman')->name('borrow.')->group(function () {
        Route::get('/daftar', function () {
            return view('borrow/index');
        })->name('index');

        Route::get('/permintaan', function () {
            return view('borrow/request');
        })->name('request');

        Route::get('/tambah', function () {
            return view('borrow/create');
        })->name('create');

        Route::get('/edit/{id}', function () {
            return view('borrow/create');
        })->name('edit');
    });

    Route::prefix('pengguna')->name('user.')->group(function () {
        Route::get('/daftar', function () {
            return view('user/index');
        })->name('index');

        Route::get('/permintaan', function () {
            return view('user/request');
        })->name('request');

        Route::get('/tambah', function () {
            return view('user/create');
        })->name('create');

        Route::get('/edit/{id}', function () {
            return view('user/create');
        })->name('edit');
    });

    Route::get('/log', function () {
        // activity()
        // ->withProperties([
        //     'object' => 'Acer ROG XXX', 
        //     'by' => '<b>Slim My Man</b>'
        // ])
        // ->log('ingin melakukan peminjaman');
        $act = Activity::all();

        return view('admin/activity', compact('act'));
    })->name('log');
});

// Route::prefix('operator')->name('operator.')->middleware('operator')->group(function () {
Route::prefix('operator')->name('operator.')->group(function () {
    Route::get('/', function () {
        return view('operator/index');
    })->name('index');
    
    Route::get('profile', function () {
        return view('profile');
    })->name('profile');

    Route::get('/peminjaman/daftar', function () {
        return view('operator/index');
    });

    Route::prefix('inventaris')->name('invent.')->group(function () {
        Route::get('inventarisir', function () {
            return view('inventory/index');
        })->name('index');

        Route::get('ruangan', function () {
            return view('inventory/room');
        })->name('room');

        Route::get('tipe', function () {
            return view('inventory/type');
        })->name('type');

        Route::get('tambah-barang', function () {
            return view('inventory/create__invent');
        })->name('create_invent');

        Route::get('edit-barang/{slug}', function () {
            return view('inventory/create__invent');
        })->name('edit_invent');

        Route::get('label', function () {
            $barcode = new BarcodeGenerator();
            $barcode->setText("0123456789");
            $barcode->setType(BarcodeGenerator::Code128);
            $barcode->setScale(2);
            $barcode->setThickness(25);
            $barcode->setFontSize(10);
            $code = $barcode->generate();

            return view('inventory/label', compact('code'));
        })->name('label');
    });

    Route::prefix('peminjaman')->name('borrow.')->group(function () {
        Route::get('/daftar', function () {
            return view('borrow/index');
        })->name('index');

        Route::get('/permintaan', function () {
            return view('borrow/request');
        })->name('request');

        Route::get('/tambah', function () {
            return view('borrow/create');
        })->name('create');
    });
});

// Route::prefix('peminjam')->name('peminjam.')->middleware('borrower')->group(function() {
Route::prefix('peminjam')->name('peminjam.')->group(function() {
    Route::get('/', function() {
        return view('peminjam/index');
    })->name('index');

    Route::get('/profile', function() {
        return view('profile');
    })->name('profile');

    Route::get('/keranjang', function() {
        return view('peminjam/keranjang');
    })->name('keranjang');

    Route::prefix('peminjaman')->name('borrow.')->group(function () {
        Route::get('daftar', function () {
            return view('peminjam/list-peminjaman');
        })->name('list');

        Route::get('detail/{id}', function () {
            return view('peminjam/detail-peminjaman');
        })->name('detail');
    });

    Route::prefix('inventaris')->name('invent.')->group(function () {
        Route::get('/', function () {
            return view('peminjam/inventaris');
        })->name('list');

        Route::get('detail/{id}', function () {
            return view('peminjam/detail-barang');
        })->name('detail');
    });
});